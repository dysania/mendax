package main

type room struct {
	id      string
	clients map[*client]bool
}

var rooms = make(map[string]*room)
