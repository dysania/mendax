package main

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
)

type client struct {
	id   string
	conn *websocket.Conn
	*room
	csend      chan string
	cbroadcast chan string
	events     map[string]func(string)
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func handleWebsocket(w http.ResponseWriter, r *http.Request) (*client, error) {
	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		fmt.Println("Error Upgrade", err)
		return nil, err
	}

	c := &client{
		id:         "",
		conn:       conn,
		room:       nil,
		csend:      make(chan string),
		cbroadcast: make(chan string),
		events:     make(map[string]func(string)),
	}

	go c.readLoop()
	go c.writeLoop()

	return c, nil
}

func (c *client) readLoop() {
	defer func() {
		c.emit("disconnected", "")
		c.conn.Close()
	}()

	for {
		// Read message from browser
		_, msg, err := c.conn.ReadMessage()

		if err != nil {
			fmt.Println("Error ReadMessage", err)
			break
		}

		event := strings.Split(string(msg), "~")

		if len(event) != 2 {
			fmt.Println("Error length of client message")
			continue
		}

		eventName := event[0]
		eventData := event[1]

		if eventName == "disconnected" {
			fmt.Println("Error invalid client event name:", eventName)
			continue
		}

		// Print the message to the console
		fmt.Printf("%s sent: %v\n", c.id, event)

		c.emit(eventName, eventData)
	}
}

func (c *client) writeLoop() {
	defer func() {
		c.conn.Close()
	}()

	for {
		select {
		case msg := <-c.csend:
			// Write message back to browser
			if err := c.conn.WriteMessage(websocket.TextMessage, []byte(msg)); err != nil {
				fmt.Println("Error WriteMessage", err)
				break
			}
		case msg := <-c.cbroadcast:
			if c.room == nil {
				continue
			}

			// Send to all clients in same room except for sender client
			for cl := range c.room.clients {
				if cl != c {
					cl.csend <- msg
				}
			}
		}
	}
}

// Send to browser
func (c *client) send(event, data string) {
	c.csend <- (event + "~" + data)
}

// Send to each browser client in c's room except c
func (c *client) broadcast(event, data string) {
	c.cbroadcast <- (event + "~" + data)
}

// Event handler for server side
func (c *client) on(event string, fn func(string)) {
	c.events[event] = fn
}

// Emit events for server side
func (c *client) emit(event, data string) {
	if fn, ok := c.events[event]; ok {
		fn(data)
	}
}
