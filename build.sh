#!/bin/env bash

# if not watch, then build and edit
if [[ "$1" != "--watch" ]] && [[ "$1" != "-w" ]]; then
  go build
  exit $?
fi

PROGRAM='mendax'
GREEN=`tput setaf 2`
RED=`tput setaf 1`
RESET=`tput sgr0`

function log() {
  echo "${GREEN}[watch] $@${RESET}"
}

function log_error() {
  echo "${RED}[watch] $@${RESET}"
}

PID=0
trap on_kill INT

function on_kill() {
  log "killing $PROGRAM"
  [[ $PID -gt 0 ]] && kill $PID
  exit 0
}

log 'watching *.go'

while true; do
  log "building $PROGRAM"
  go build
  STATUS=$?

  if [[ $STATUS == 0 ]]; then
    log "starting $PROGRAM"
    ./$PROGRAM &
    PID=$!
  else
    log_error "error building $PROGRAM"
    PID=0
  fi

  log 'restarting due to:' `inotifywait -qe close_write *.go`
  if [[ $PID -gt 0 ]]; then
    kill $PID
    # bash prints a Terminated message, this suppresses that
    wait $PID 2>/dev/null
  fi
done
