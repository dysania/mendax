package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		c, err := handleWebsocket(w, r)

		if err != nil {
			return
		}

		// When a client wants to connect for first time
		c.on("connect", func(_ string) {
			if len(c.id) > 0 {
				fmt.Println(c.id, "already connected")
				return
			}

			c.id = genID(6)
			fmt.Println(c.id, "connected!!!")

			// Send client its id
			c.send("id", c.id)
		})

		// Reconnect due to timeout, network error, etc
		c.on("reconnect", func(clientID string) {
			if len(c.id) > 0 {
				fmt.Println(c.id, "already connected")
				return
			}

			cl := (*client)(nil)

			for _, r := range rooms {
				for cc, connected := range r.clients {
					if cc.id == clientID && !connected {
						cl = cc
						break
					}
				}

				if cl != nil {
					break
				}
			}

			if cl != nil {
				c = cl
				fmt.Println(c.id, "reconnected!!!")
			}
		})

		// This one is special and is emitted from server side when client disconnects
		c.on("disconnected", func(_ string) {
			fmt.Println(c.id, "disconnected!!!")

			if c.room != nil {
				c.room.clients[c] = false
			}
		})

		// Echo back message
		c.on("msg", func(data string) {
			c.send("msg", data)
		})

		c.on("new-room", func(_ string) {
			if c.room != nil {
				return
			}

			roomID := genID(6)

			rooms[roomID] = &room{
				id:      roomID,
				clients: make(map[*client]bool),
			}

			c.room = rooms[roomID]
			c.room.clients[c] = true
		})

		c.on("join-room", func(roomId string) {
			if c.room != nil {
				return
			}

			if r, ok := rooms[roomId]; ok {
				c.room = r
				c.room.clients[c] = true
			}
		})

		c.on("leave-room", func(_ string) {
			if c.room == nil {
				return
			}

			delete(c.room.clients, c)

			// If last person leaves, delete room
			if len(c.room.clients) == 0 {
				delete(rooms, c.room.id)
			}

			c.room = nil
		})
	})

	fileServer := http.FileServer(http.Dir("static/"))
	http.Handle("/", fileServer)

	fmt.Println("=> Running at :8080")

	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Println(err)
	}
}

func genID(size int) string {
	id := make([]byte, size)
	alpha := "abcdefghijklmnopqrstuvwx0123456789"

	for i := 0; i < size; i++ {
		id[i] = alpha[rand.Intn(len(alpha))]
	}

	return string(id)
}
