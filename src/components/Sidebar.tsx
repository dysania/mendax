import React from "react";
import { Link, List, ListItem } from "@material-ui/core";
import { useHistory } from "react-router-dom";

export default function Sidebar({ onChange }: { onChange: () => void }) {
  const hist = useHistory();
  return (
    <List style={{ minWidth: "40vw" }}>
      <ListItem>
        <Link
          onClick={() => {
            onChange();
            hist.push("/");
          }}
        >
          Home
        </Link>
      </ListItem>
      <ListItem>
        <Link
          onClick={() => {
            onChange();
            hist.push("/about");
          }}
        >
          About
        </Link>
      </ListItem>
    </List>
  );
}
