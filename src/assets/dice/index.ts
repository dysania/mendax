import one from "./one.svg";
import two from "./two.svg";
import three from "./three.svg";
import four from "./four.svg";
import five from "./five.svg";
import six from "./six.svg";

export { one, two, three, four, five, six };
