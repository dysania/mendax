import firebase from "firebase/app";
import "firebase/firestore";
const firebaseConfig = {
  apiKey: "AIzaSyCgApFp_kM-Ow3NxetQ3JCKVAQQ5hZ5yBE",
  authDomain: "dysania-mendax.firebaseapp.com",
  databaseURL: "https://dysania-mendax.firebaseio.com",
  projectId: "dysania-mendax",
  storageBucket: "dysania-mendax.appspot.com",
  messagingSenderId: "946598428126",
  appId: "1:946598428126:web:1932fb74be1d1935ae314f",
};
export default firebase.initializeApp(firebaseConfig).firestore();

const firestore = firebase.firestore;
export { firestore };
