import uuid from "./uuid";

const KEY = "userId";

export default function getUseId() {
  const ss = window.localStorage;
  let userId = ss.getItem(KEY);

  if (userId === null) {
    userId = uuid();
    ss.setItem(KEY, userId);
  }
  return userId;
}
