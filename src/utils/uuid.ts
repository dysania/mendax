export default function uuid() {
  let id = "";
  for (let i = 0; i < 8; i++) {
    id += genPart();
  }
  return id;
}

function genPart() {
  return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}
