import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { enableMapSet } from "immer";
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Drawer,
  CssBaseline,
} from "@material-ui/core";
import { Menu as MenuIcon } from "@material-ui/icons";
import Home from "./scenes/Home";
import Setup from "./scenes/Setup";
import Sidebar from "./components/Sidebar";
import About from "./scenes/About";
import Play from "./scenes/Play";

enableMapSet();

export default function Root() {
  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <Router>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <IconButton
            onClick={() => {
              setMenuOpen(!menuOpen);
            }}
          >
            <MenuIcon></MenuIcon>
          </IconButton>
          <Typography>Mendax</Typography>
        </Toolbar>
        <Drawer
          anchor="left"
          open={menuOpen}
          onClose={() => {
            setMenuOpen(!menuOpen);
          }}
        >
          <Sidebar
            onChange={() => {
              setMenuOpen(!menuOpen);
            }}
          ></Sidebar>
        </Drawer>
      </AppBar>
      <Switch>
        <Route path="/play/:gameId">
          <Play></Play>
        </Route>
        <Route path="/setup/:gameId">
          <Setup></Setup>
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/">
          <Home></Home>
        </Route>
      </Switch>
    </Router>
  );
}
