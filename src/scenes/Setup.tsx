import React, { useState, useEffect } from "react";
import {
  Button,
  TextField,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { HourglassEmpty, AccountCircle } from "@material-ui/icons";
import db from "../firebase";
import { DocumentData, DocumentSnapshot } from "@firebase/firestore-types";
import { useParams, useHistory } from "react-router-dom";
import getUserId from "../utils/getUserId";
import _ from "lodash";

export default function Setup() {
  const { gameId } = useParams<{ gameId: string }>();
  const hist = useHistory();
  const [playerName, setPlayerName] = useState<string | null>(null);
  const [playerNameIsSet, setPlayerNameIsSet] = useState(false);
  const [game, setGame] = useState<null | DocumentSnapshot<DocumentData>>(null);

  let isAdmin = false;

  useEffect(() => {
    const gameRef = db.collection("games").doc(gameId);

    (async () => {
      let gameDoc = await gameRef.get();
      setGame(gameDoc);
    })();

    return gameRef.onSnapshot((doc) => {
      setGame(doc);
      let name = doc.data()?.players?.[getUserId()]?.name;
      if (name) {
        setPlayerNameIsSet(true);
        setPlayerName(name);
      }
    });
  }, [gameId]);

  if (game === null) {
    // we loading
    return (
      <Grid
        container
        alignItems="center"
        direction="column"
        style={{ padding: "15% 10%" }}
      >
        <HourglassEmpty fontSize="large" />
      </Grid>
    );
  } else {
    isAdmin = game.data()?.admin === getUserId();
    if (game.data()?.state === "playing") {
      if (!playerNameIsSet) {
        return <p>The game started without you</p>;
      }
      hist.push(`/play/${gameId}`);
      return null;
    }
  }

  const playerNameInvalid =
    playerName !== null && (playerName.length < 3 || playerName.length > 15);

  let playerNameInvalidReason;
  if ((playerName?.length ?? 3) < 3) {
    playerNameInvalidReason = "Name must be at least 3 characters";
  } else if ((playerName?.length ?? 15) > 15) {
    playerNameInvalidReason = "Name cannot be greater than 15 characters";
  }

  const otherPlayers = () => {
    let players = game.data()?.players;
    if (!players) throw Error("There should be players!");

    return (
      <List>
        {Object.entries<{ name: string }>(players)
          .filter(([id]) => {
            return id !== getUserId();
          })
          .map(([id, info]) => {
            return (
              <ListItem key={id}>
                <ListItemIcon>
                  <AccountCircle />
                </ListItemIcon>
                <ListItemText style={{ minWidth: "50vw" }}>
                  {info?.name}
                </ListItemText>
              </ListItem>
            );
          })}
      </List>
    );
  };

  return (
    <Grid
      container
      alignItems="center"
      direction="column"
      style={{ padding: "15% 10%" }}
    >
      <Grid item>
        <p>Let's set up your game</p>
      </Grid>
      <Grid item>
        <TextField
          error={playerNameInvalid}
          helperText={playerNameInvalid ? playerNameInvalidReason : null}
          variant="outlined"
          placeholder="Enter your name"
          disabled={playerNameIsSet}
          value={playerName ?? ""}
          onChange={(e) => {
            setPlayerName(e.target.value);
          }}
        ></TextField>
      </Grid>
      {!playerNameIsSet && !playerNameInvalid && playerName ? (
        <Grid item style={{ marginTop: "1rem" }}>
          <Button
            variant="contained"
            color="primary"
            onClick={async () => {
              if (isAdmin) {
                await game.ref.set(
                  {
                    state: "adding-players",
                    players: { [getUserId()]: { name: playerName } },
                  },
                  { merge: true }
                );
              } else {
                await game.ref.set(
                  {
                    players: { [getUserId()]: { name: playerName } },
                  },
                  { merge: true }
                );
              }
              setPlayerNameIsSet(true);
            }}
          >
            Set name
          </Button>
        </Grid>
      ) : null}
      {playerNameIsSet && isAdmin ? (
        <Grid item style={{ marginTop: "1rem" }}>
          <p>
            Now copy this link to share with others you want to play with:{" "}
            <a
              href={window.location.href}
              onClick={(e) => {
                e.preventDefault();
              }}
            >
              LINK
            </a>
          </p>
          <p>
            They can also type the code <b>{gameId}</b>
          </p>
          <p>As they join, their names will show below.</p>
        </Grid>
      ) : null}
      {playerNameIsSet && !isAdmin ? (
        <Grid item>
          <p>
            Waiting for other players. Once everyone has joined the person who
            created this game can press start.
          </p>
        </Grid>
      ) : null}
      {playerNameIsSet ? <Grid item>{otherPlayers()}</Grid> : null}
      {playerNameIsSet &&
      isAdmin &&
      Object.keys(game.data()?.players)?.length > 1 ? (
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={async () => {
              const setObj: {
                state: string;
                moves: object[];
                turnPlayer: null | string;
                players: { [key: string]: object };
              } = {
                state: "playing",
                moves: [],
                turnPlayer: null,
                players: {},
              };

              let players = game.data()?.players;
              let playerIds: string[] = Object.keys(players);
              playerIds = _.shuffle(playerIds);

              for (let i = 0; i < playerIds.length; i++) {
                if (i === 0) {
                  setObj.turnPlayer = playerIds[i];
                }
                setObj.players[playerIds[i]] = {
                  dice: Array(5)
                    .fill(0)
                    .map(() => _.random(1, 6)),
                  orderPlacement: i,
                };
              }

              await game.ref.set(setObj, { merge: true });
            }}
          >
            We're ready to play
          </Button>
        </Grid>
      ) : null}
    </Grid>
  );
}
