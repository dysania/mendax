import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  Button,
  TextField,
  Grid,
  Snackbar,
  Card,
  CardContent,
  Typography,
  Fab,
  Drawer,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  List,
  ListItem,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import Confetti from "react-confetti";
import {
  HourglassEmpty,
  CropSquareRounded,
  SentimentDissatisfied,
  Equalizer,
  ExpandMore,
  Visibility,
  VisibilityOff,
} from "@material-ui/icons";
import * as dice from "../assets/dice";
import db from "../firebase";
import getUserId from "../utils/getUserId";
import { DocumentSnapshot, Timestamp } from "@firebase/firestore-types";
import produce from "immer";
import _ from "lodash";
import getUseId from "../utils/getUserId";

export default function Play() {
  const { gameId } = useParams<{ gameId: string }>();
  const [game, setGame] = useState<null | DocumentSnapshot<GameDocument>>(null);
  const [diceHidden, setDiceHidden] = useState(true);
  const [input, setInput] = useState<InputState>(defaultInput);
  const [alert, setAlert] = useState<AlertState>(defaultAlert);
  const [statsMenu, setStatsMenu] = useState(false);
  const [windowInfo, setWindowInfo] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    const onResize = () => {
      setWindowInfo({
        height: window.innerHeight,
        width: window.innerWidth,
      });
    };

    window.addEventListener("resize", onResize);
    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  useEffect(() => {
    const gameRef = db.collection("games").doc(gameId);

    (async () => {
      let gameDoc = await gameRef.get();
      setGame(gameDoc as DocumentSnapshot<GameDocument>);
    })();

    return gameRef.onSnapshot((doc) => {
      setGame(doc as DocumentSnapshot<GameDocument>);
    });
  }, [gameId]);

  const gameData = game?.data();

  if (game === null || gameData === undefined) {
    // we loading
    return (
      <Grid
        container
        alignItems="center"
        direction="column"
        style={{ padding: "15% 10%" }}
      >
        <HourglassEmpty fontSize="large" />
      </Grid>
    );
  }

  const currentPlayer = gameData.players[gameData.turnPlayer ?? ""];
  const lastMove = _.last(gameData.moves ?? []) ?? null;
  const secondLastMove = _.nth(gameData.moves ?? [], -2) ?? null;
  const isRoundStart = (lastMove?.type ?? "call") === "call";
  const playersStartingWithTurnPlayer = getPlayersByTurn(
    gameData,
    gameData.turnPlayer
  );
  const oofYouLost = (gameData.players?.[getUserId()]?.dice?.length ?? 0) === 0;
  const yayYouWon =
    !oofYouLost &&
    getPlayersByTurn(gameData, getUserId()).every(([id, player]) => {
      if (id === getUserId()) {
        return true;
      }
      return !player.dice.length;
    });
  const myTurn = gameData.turnPlayer === getUserId() && !yayYouWon;

  return (
    <>
      <Grid
        container
        alignItems="center"
        direction="column"
        style={{ padding: "10% 10%" }}
      >
        {genMainDisplay()}
        {genCurrentAction()}
        {lastMove === null ? null : (
          <Grid item style={{ marginTop: "5vh" }}>
            <Card style={{ padding: "5vw" }}>
              <CardContent>
                <Typography
                  style={{ marginBottom: ".5rem" }}
                  color="textSecondary"
                >
                  Last Play Info
                </Typography>
                {lastMove.type === "raise" ? (
                  <>
                    <Typography>
                      {gameData.players[lastMove.doneBy].name} bet{" "}
                      {lastMove.bet?.quantity}{" "}
                      <span>
                        <img
                          style={{ height: "1em", width: "1em" }}
                          src={mapNumToDice(lastMove.bet?.value ?? 0)}
                        ></img>
                      </span>
                    </Typography>
                    <br />
                    {secondLastMove?.type === "raise" ? (
                      <Typography color="textSecondary">
                        Raised from{" "}
                        {gameData.players[secondLastMove.doneBy].name}'s bet of{" "}
                        {secondLastMove.bet?.quantity}{" "}
                        <span>
                          <img
                            style={{ height: "1em", width: "1em" }}
                            src={mapNumToDice(secondLastMove.bet?.value ?? 0)}
                          ></img>
                        </span>
                      </Typography>
                    ) : null}
                  </>
                ) : (
                  <>
                    <Typography>
                      {gameData.players[lastMove.doneBy].name} called{" "}
                      {gameData.players[secondLastMove?.doneBy ?? ""].name}
                      's bet of {secondLastMove?.bet?.quantity}{" "}
                      <span>
                        <img
                          style={{ height: "1em", width: "1em" }}
                          src={mapNumToDice(secondLastMove?.bet?.value ?? 0)}
                        ></img>
                      </span>
                    </Typography>
                    <Typography color="textSecondary">
                      There were{" "}
                      {
                        ((lastMove.stats?.quantityOfEach as unknown) as {
                          [field: string]: number;
                        })[numToWord(secondLastMove?.bet?.value ?? 0)]
                      }{" "}
                      of the needed {secondLastMove?.bet?.quantity}{" "}
                      <span>
                        <img
                          style={{ height: "1em", width: "1em" }}
                          src={mapNumToDice(secondLastMove?.bet?.value ?? 0)}
                        ></img>
                      </span>
                    </Typography>
                  </>
                )}
              </CardContent>
            </Card>
          </Grid>
        )}
      </Grid>
      <Snackbar
        open={alert.open}
        autoHideDuration={10000}
        onClose={onAlertClose}
      >
        <Alert severity={alert.sev} onClose={onAlertClose}>
          {alert.msg}
        </Alert>
      </Snackbar>
      <Fab
        style={{ position: "absolute", bottom: "1.5rem", left: "1.5rem" }}
        onClick={() => {
          setDiceHidden(!diceHidden);
        }}
      >
        {diceHidden ? <Visibility /> : <VisibilityOff />}
      </Fab>
      <Fab
        style={{ position: "absolute", bottom: "1.5rem", right: "1.5rem" }}
        onClick={() => {
          setStatsMenu(true);
        }}
      >
        <Equalizer />
      </Fab>
      <Drawer
        open={statsMenu}
        anchor="right"
        onClose={() => {
          setStatsMenu(false);
        }}
      >
        <Accordion style={{ minWidth: "70vw" }}>
          <AccordionSummary expandIcon={<ExpandMore />}>
            <Typography>Player Order</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              {playersStartingWithTurnPlayer.map(([id, player]) => {
                return (
                  <ListItem key={id}>
                    <Typography
                      color={
                        player.dice.length ? "textPrimary" : "textSecondary"
                      }
                      style={id === getUseId() ? { fontWeight: "bold" } : {}}
                    >
                      {player.name} ({player.dice.length} dice)
                    </Typography>
                  </ListItem>
                );
              })}
            </List>
          </AccordionDetails>
        </Accordion>
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMore />}>
            <Typography>Game Stats</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <List>
              <ListItem>
                <Typography>
                  There are{" "}
                  {playersStartingWithTurnPlayer.reduce((acc, [, p]) => {
                    return acc + p.dice.length;
                  }, 0)}{" "}
                  total dice in play
                </Typography>
              </ListItem>
              <ListItem>
                <Typography>
                  1/3 of that total is{" "}
                  {(
                    playersStartingWithTurnPlayer.reduce((acc, [, p]) => {
                      return acc + p.dice.length;
                    }, 0) / 3
                  ).toLocaleString(undefined, { maximumFractionDigits: 1 })}
                </Typography>
              </ListItem>
            </List>
          </AccordionDetails>
        </Accordion>
      </Drawer>
    </>
  );

  function onAlertClose(...[, reason = ""]) {
    if (reason === "clickaway") {
      return;
    }
    setAlert(
      produce((a: AlertState) => {
        a.open = false;
      })
    );
  }

  function genCurrentAction() {
    if (oofYouLost || yayYouWon) return null;
    return myTurn ? (
      <>
        <Grid item>
          <p>It is your turn</p>
        </Grid>
        <Grid item container>
          <Grid item xs={6}>
            <TextField
              type="number"
              variant="outlined"
              placeholder="Quantity"
              error={input.dieQuantity.invalid}
              helperText={input.dieQuantity.errorText ?? undefined}
              onChange={(e) => {
                let v = Number(e.target.value);
                setInput(
                  produce((i: InputState) => {
                    const dq = i.dieQuantity;

                    if (isNaN(v)) {
                      dq.errorText = "Must be a number";
                      dq.invalid = true;
                      return;
                    }
                    if (!_.isInteger(v)) {
                      dq.errorText = "Must be whole";
                      dq.invalid = true;
                      return;
                    }
                    if (v < 1) {
                      dq.errorText = "Must be positive";
                      dq.invalid = true;
                      return;
                    }

                    // game correctness checks
                    if (!isRoundStart) {
                      const lastQuantity = lastMove?.bet?.quantity ?? Infinity;
                      if (v < lastQuantity) {
                        dq.errorText = `Must be at least ${lastQuantity} (from the last bet)`;
                        dq.invalid = true;
                        return;
                      }
                    }

                    dq.errorText = null;
                    dq.invalid = false;
                    dq.val = v;
                  })
                );
                setAlert(
                  produce((a: AlertState) => {
                    a.open = false;
                  })
                );
              }}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              type="number"
              variant="outlined"
              placeholder="Value"
              error={input.dieValue.invalid}
              helperText={input.dieValue.errorText ?? undefined}
              onChange={(e) => {
                let v = Number(e.target.value);
                setInput(
                  produce((i: InputState) => {
                    const dv = i.dieValue;

                    // basic correctness checks
                    if (isNaN(v)) {
                      dv.errorText = "Must be a number";
                      dv.invalid = true;
                      return;
                    }
                    if (!_.isInteger(v)) {
                      dv.errorText = "Must be whole";
                      dv.invalid = true;
                      return;
                    }
                    if (v === 1) {
                      dv.errorText = "Must be 2-6. One's are wild";
                      dv.invalid = true;
                      return;
                    }
                    if (v < 2 || v > 6) {
                      dv.errorText = "Must be 2-6";
                      dv.invalid = true;
                      return;
                    }

                    dv.errorText = null;
                    dv.invalid = false;
                    dv.val = v;
                  })
                );
                setAlert(
                  produce((a: AlertState) => {
                    a.open = false;
                  })
                );
              }}
            />
          </Grid>
        </Grid>
        {inputReady(input) ? (
          <Grid item>
            <p>
              "I bet there are {input.dieQuantity.val}{" "}
              <span>
                <img
                  style={{ height: "1em", width: "1em" }}
                  src={mapNumToDice(input.dieValue.val ?? 0)}
                ></img>
              </span>
              s"
            </p>
          </Grid>
        ) : null}
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            style={{ margin: "1rem" }}
            onClick={async () => {
              // make ts happy
              if (gameData === undefined)
                throw Error("There should be game data by now");
              if (game === null) throw Error("The game should be set by now");

              if (input.dieQuantity.invalid || input.dieValue.invalid)
                return setAlert(
                  produce((a: AlertState) => {
                    a.msg = "Your input must be valid";
                    a.open = true;
                    a.sev = "error";
                  })
                );
              if (input.dieQuantity.val === null || input.dieValue.val === null)
                return setAlert(
                  produce((a: AlertState) => {
                    a.msg = "Please input your bet";
                    a.open = true;
                    a.sev = "error";
                  })
                );

              const inputQuantity = input.dieQuantity.val ?? -1;
              const inputValue = input.dieValue.val ?? -1;

              // do some game checks
              if (isRoundStart) {
              } else {
                const lastQuantity = lastMove?.bet?.quantity ?? Infinity;
                const lastValue = lastMove?.bet?.value ?? Infinity;
                if (lastQuantity === inputQuantity && lastValue >= inputValue) {
                  return setAlert(
                    produce((a: AlertState) => {
                      a.msg = "Your bet must be greater than the last";
                      a.open = true;
                      a.sev = "error";
                    })
                  );
                }
              }

              setInput(defaultInput);

              //  save
              await game.ref.set(
                produce(gameData, (g) => {
                  g.moves.push({
                    doneBy: getUserId(),
                    type: "raise",
                    bet: {
                      quantity: inputQuantity,
                      value: inputValue,
                    },
                  });

                  const survivingPlayers = Object.entries(g.players)
                    .filter(([_, player]) => {
                      return player.dice.length > 0;
                    })
                    .sort(([, l], [, r]) => {
                      return l.orderPlacement - r.orderPlacement;
                    });
                  const myIdx = survivingPlayers.findIndex(([id]) => {
                    return id === getUserId();
                  });
                  g.turnPlayer = getNextInRing(survivingPlayers, myIdx)[0];
                }),
                { merge: true }
              );
            }}
          >
            raise
          </Button>
        </Grid>
        {isRoundStart ? null : (
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              style={{ margin: "1rem" }}
              onClick={async () => {
                // make ts happy
                if (gameData === undefined)
                  throw Error("There should be game data by now");
                if (game === null) throw Error("Game should be set by now");

                //sanity checks
                if (lastMove === null)
                  throw Error("Needs to be a last move to call");

                const lastQuantity = lastMove.bet?.quantity ?? Infinity;
                const lastValue = lastMove.bet?.value ?? Infinity;
                const lastMovePlayer = lastMove.doneBy;

                const stats = {
                  two: 0,
                  three: 0,
                  four: 0,
                  five: 0,
                  six: 0,
                } as { [field: string]: number };

                for (const val in stats) {
                  stats[val] = Object.values(gameData.players)
                    .map((p) => p.dice)
                    .reduce((acc, dice) => {
                      return (
                        acc +
                        dice.filter((d) => d === 1 || d === wordToNum(val))
                          .length
                      );
                    }, 0);
                }

                setInput(defaultInput);

                const totalNumOfValue = stats[numToWord(lastValue)];

                const wasGoodCall = totalNumOfValue < lastQuantity;

                await game.ref.set(
                  produce(gameData, (g: GameDocument) => {
                    g.moves.push({
                      doneBy: getUserId(),
                      type: "call",
                      stats: {
                        wasGoodCall,
                        quantityOfEach: (stats as unknown) as QuantityOfEach,
                      },
                    });

                    if (wasGoodCall) {
                      g.players[lastMovePlayer].dice.pop();
                      // g.turnPlayer = lastMovePlayer;

                      let nextPlayer = getPlayersByTurn(g, lastMovePlayer).find(
                        ([, p]) => {
                          return p.dice.length > 0;
                        }
                      );

                      g.turnPlayer = nextPlayer ? nextPlayer[0] : "";
                    } else {
                      g.players[getUseId()].dice.pop();

                      let nextPlayer = getPlayersByTurn(g, getUseId()).find(
                        ([, p]) => {
                          return p.dice.length > 0;
                        }
                      );

                      g.turnPlayer = nextPlayer ? nextPlayer[0] : "";
                    }

                    Object.values(g.players).forEach((p) => {
                      for (let i = 0; i < p.dice.length; i++) {
                        p.dice[i] = _.random(1, 6);
                      }
                    });
                  }),
                  { merge: true }
                );
              }}
            >
              call
            </Button>
          </Grid>
        )}
      </>
    ) : (
      <Grid item>
        <p>It is {currentPlayer?.name}'s turn</p>
      </Grid>
    );
  }

  function genMainDisplay() {
    if (!gameData) throw Error("I need gameData");

    if (oofYouLost) {
      return (
        <Grid item container alignItems="center" direction="column">
          <SentimentDissatisfied fontSize="large" />
          <h2>You lost</h2>
          <Confetti
            width={windowInfo.width}
            height={windowInfo.height}
            numberOfPieces={20}
            gravity={0.02}
            colors={["#111", "#333", "#555", "#777"]}
            opacity={0.8}
          ></Confetti>
        </Grid>
      );
    }
    if (yayYouWon) {
      return (
        <Grid item>
          <h2>You won!</h2>
          <Confetti
            width={windowInfo.width}
            height={windowInfo.height}
            numberOfPieces={100}
          ></Confetti>
        </Grid>
      );
    }
    return (
      <Grid item container justify="center" alignItems="center">
        {_.sortBy(gameData.players[getUserId()].dice).map(
          (n: number, i, orig) => {
            const style = {
              height: "15vw",
              width: "15vw",
              justifySelf: "center",
              marginLeft: "auto",
              marginRight: "auto",
              display: "block",
            };
            return (
              <Grid
                style={{ paddingTop: ".25em" }}
                item
                xs={orig.length > 4 ? 4 : 3}
                key={i}
                alignContent="center"
              >
                {diceHidden ? (
                  <CropSquareRounded style={style} />
                ) : (
                  <img style={style} src={mapNumToDice(n)}></img>
                )}
              </Grid>
            );
          }
        )}
      </Grid>
    );
  }
}

function inputReady(i: InputState): boolean {
  return !!(
    i.dieQuantity.val &&
    !i.dieQuantity.invalid &&
    i.dieValue.val &&
    !i.dieValue.invalid
  );
}

function getNextInRing<T>(ar: T[], idx: number): T {
  if (idx === ar.length - 1) {
    return ar[0];
  }
  return ar[idx + 1];
}

function numToWord(num: number): string {
  if (num === 1) return "one";
  if (num === 2) return "two";
  if (num === 3) return "three";
  if (num === 4) return "four";
  if (num === 5) return "five";
  if (num === 6) return "six";
  throw Error(`Num (${num}) is not on a D6!`);
}

function wordToNum(word: string): number {
  if (word === "one") return 1;
  if (word === "two") return 2;
  if (word === "three") return 3;
  if (word === "four") return 4;
  if (word === "five") return 5;
  if (word === "six") return 6;
  throw Error(`Word (${word}) is not on a D6!`);
}

function mapNumToDice(num: number) {
  if (num === 1) return dice.one;
  if (num === 2) return dice.two;
  if (num === 3) return dice.three;
  if (num === 4) return dice.four;
  if (num === 5) return dice.five;
  if (num === 6) return dice.six;
  throw Error(`Num (${num}) is not on a D6!`);
}

function getPlayersByTurn(
  g: GameDocument,
  startPlayer: string
): [string, Player][] {
  let players = Object.entries(g.players);

  return produce(players, (playersDraft) => {
    playersDraft.sort(([, l], [, r]) => {
      return l.orderPlacement - r.orderPlacement;
    });
    while (
      playersDraft.findIndex(([pid]) => {
        return pid === startPlayer;
      }) !== 0
    ) {
      let front = playersDraft.shift();
      if (front === undefined) throw Error("This array should have items!");
      playersDraft.push(front);
    }
  });
}

interface InputState {
  dieQuantity: {
    val: number | null;
    invalid: boolean;
    errorText: string | null;
  };
  dieValue: {
    val: number | null;
    invalid: boolean;
    errorText: string | null;
  };
}

const defaultInput: InputState = {
  dieQuantity: {
    val: null,
    invalid: false,
    errorText: null,
  },
  dieValue: {
    val: null,
    invalid: false,
    errorText: null,
  },
};

interface AlertState {
  msg: string | null;
  open: boolean;
  sev: "error" | "info" | "success" | "warning";
}

const defaultAlert: AlertState = {
  msg: null,
  open: false,
  sev: "error",
};

interface GameDocument {
  admin: string;
  created: Timestamp;
  moves: Move[];
  players: { [field: string]: Player };
  state: string;
  turnPlayer: string;
}

interface Move {
  doneBy: string;
  type: "raise" | "call";
  bet?: {
    quantity: number;
    value: number;
  };
  stats?: {
    wasGoodCall: boolean;
    quantityOfEach: QuantityOfEach;
  };
}

interface QuantityOfEach {
  two: number;
  three: number;
  four: number;
  five: number;
  six: number;
}

interface Player {
  name: string;
  dice: number[];
  orderPlacement: number;
}
