import React, { useState } from "react";
import { Button, TextField, Grid, Typography } from "@material-ui/core";
import db, { firestore } from "../firebase";
import { useHistory } from "react-router-dom";
import getUserId from "../utils/getUserId";

const onlyNumbers = /^[0-9]{2,10}$/;

export default function Home() {
  const [gameId, setGameId] = useState<string | null>(null);
  const hist = useHistory();
  let entryInvalid = false;
  let showSubmit = false;

  if (gameId !== null && gameId !== "") {
    entryInvalid = !onlyNumbers.test(gameId);
    showSubmit = !entryInvalid;
  }

  return (
    <Grid
      container
      style={{ marginTop: "20%" }}
      alignItems="center"
      direction="column"
    >
      <Grid item>
        <Button
          variant="contained"
          color="primary"
          onClick={async () => {
            let gameId = await createGame();
            hist.push(`/setup/${gameId}`);
          }}
        >
          Start a new game
        </Button>
      </Grid>
      <hr style={{ width: "35%", margin: "10%" }} />
      <Grid item>
        <Typography align="center">Or to join an existing game:</Typography>
        <TextField
          error={entryInvalid}
          helperText={entryInvalid ? "Enter a valid ID" : null}
          variant="outlined"
          placeholder="Enter the Game ID here"
          onChange={(e) => {
            setGameId(e.target.value);
          }}
        ></TextField>
      </Grid>
      {showSubmit ? (
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            style={{ marginTop: "1rem" }}
            onClick={() => {
              if (!entryInvalid) hist.push(`/setup/${gameId}`);
            }}
          >
            Enter Game
          </Button>
        </Grid>
      ) : null}
    </Grid>
  );
}
const gamesMetaRef = db.collection("meta").doc("games");
const gameCollection = db.collection("games");

async function createGame(): Promise<number> {
  const gameId = await db.runTransaction(async (transaction) => {
    let meta = await transaction.get(gamesMetaRef);
    let currentId: number = meta.data()?.lastId + 1;
    transaction.update(gamesMetaRef, { lastId: currentId });
    return currentId;
  });
  await gameCollection.doc(gameId.toString()).set({
    created: firestore.FieldValue.serverTimestamp(),
    state: "created",
    admin: getUserId(),
  });
  return gameId;
}
